//: Playground - noun: a place where people can play

import UIKit


func Euclid(a: Int, b: Int) -> Int{
    if (b==0){
        return a;
    }
    else{
        return Euclid(a: b,b: a%b )
    }
}

var x = Euclid(a: 3768%1701, b: 1701)
print(x)

